<?php

namespace Runroom\GildedRose;
use Runroom\GildedRose\GildedRoseComponent;
class GildedRose {

    private $items;

    function __construct($items) {
        $this->items = $items;
    }

    function update_quality() {
        $gildedRose = new GildedRoseComponent();
        foreach ($this->items as $item) {
            $gildedRose->logicName($item);
            if ($item->sell_in < 0) $gildedRose->logicSellIn($item);
        }
    }
}
