<?php
/**
 * Created by PhpStorm.
 * User: Tartar
 * Date: 27/10/2017
 * Time: 18:00
 */

namespace Runroom\GildedRose;

class GildedRoseComponent
{
    function logicName($item){
        if ($item->name != 'Aged Brie' and $item->name != 'Backstage passes to a TAFKAL80ETC concert') {
            if ($item->quality > 0 and $item->name != 'Sulfuras, Hand of Ragnaros') $item->quality--;
        } elseif ($item->quality < 50) {
            $item->quality++;
            if ($item->name == 'Backstage passes to a TAFKAL80ETC concert') {
                if ($item->sell_in < 11 and $item->quality < 50) $item->quality++;
                if ($item->sell_in < 6 and $item->quality < 50) $item->quality++;
            }
        }
        if ($item->name != 'Sulfuras, Hand of Ragnaros') {
            $item->sell_in--;
        }
    }

    function logicSellIn($item){
        if ($item->name != 'Aged Brie') {
            if ($item->name == 'Backstage passes to a TAFKAL80ETC concert'){
                $item->quality = 0;
            }elseif($item->quality > 0 and $item->name != 'Sulfuras, Hand of Ragnaros'){
                $item->quality--;
            }
        } elseif ($item->quality < 50) {
             $item->quality++;
        }
    }
}